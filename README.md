Linkit media file direct
========================
Provides a new substitution type for Linkit that will automatically
output a direct download link for the media if its a single file, and
link to the media page if there are multiple files attached to the
media entity.

Installation
------------

* Normal module installation procedure. See
https://www.drupal.org/documentation/install/modules-themes/modules-8


Configuration
------------

Visiting a Linkit editing profile at /admin/config/content/linkit,
clicking Manage matchers then add/edit a 'Media' matcher, you will 
now see the Substitution Type selection drop down which is normally
not shown.

Choosing File media direct URL will enable the functionality of this
module.

